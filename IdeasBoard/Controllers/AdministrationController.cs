﻿using IdeasBoard.Models;
using IdeasBoard.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IdeasBoard.Controllers
{
    public class AdministrationController : Controller
    {
        private ApplicationDbContext context;       

        public AdministrationController()
        {
            context = new ApplicationDbContext();
        }

        [Authorize(Roles = "Owner,Admin")]
        public ActionResult UsersWithRoles()
        {
            ViewBag.UserColumns = new List<string> { "Name", "Email", "Role" };
            var usersWithRoles = (from user in context.Users
                                  select new
                                  {
                                      UserId = user.Id,
                                      Name = user.Name,
                                      Email = user.Email,
                                      Roles= (from userRole in user.Roles
                                                   join role in context.Roles on userRole.RoleId
                                                   equals role.Id
                                                   select role.Name).ToList()
                                  }).ToList().Select(p => 
                                  new UserRoleViewModel()
                                  {
                                      UserId = p.UserId,
                                      Name = p.Name,
                                      Email = p.Email,
                                      Role = string.Join(",", p.Roles)
                                  });

            return View(usersWithRoles);
        }
    }
}