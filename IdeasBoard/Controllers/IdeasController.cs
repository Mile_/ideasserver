﻿using IdeasBoard.Models;
using IdeasBoard.Utilities;
using IdeasBoard.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IdeasBoard.Controllers
{
    public class IdeasController : Controller
    {
        private ApplicationUserManager _userManager;

        private ApplicationDbContext context;

        public IdeasController()
        {
            context = new ApplicationDbContext();
        }      

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

            private set
            {
                _userManager = value;
            }
        }


        // GET: Ideas
        [Authorize]
        public ActionResult Index()
        {
            return View(context.Ideas.Include(i => i.Votes).ToList());
        }

        // GET: Ideas/Details/5
        [Authorize]
        public async Task<ActionResult> Details(int id=0)
        {
            try
            {
                Idea idea = await context.Ideas.FindAsync(id);

                if (idea == null) return RedirectToAction("Index");

               ICollection<ApplicationUser> users = UserManager.Users.ToList();
               ApplicationUser owner = (from user 
                                        in users
                                        where user.Id == idea.UserID
                                        select user)
                                        .FirstOrDefault();

                ICollection<IdeaUserDetails> usersWhoVoted = (from user
                                                              in users
                                                              where idea.Votes.Any(vote => vote.UserID == user.Id)
                                                              select new IdeaUserDetails
                                                              {
                                                                  UserName = user.Name,
                                                                  VotedOn = DateTime.Now

                                                              }).ToList();

                IdeaDetailsViewModel ideaDetailsModel = new IdeaDetailsViewModel
                {
                    IdeaID = idea.IdeaID,
                    Name = idea.Name,
                    Description = idea.Description,
                    OwnerName = owner.Name,
                    OwnerEmail = owner.Email,
                    Supplies = idea.Supplies,
                    VoteCount = idea.Votes.Count(),
                    UsersWhoVoted = usersWhoVoted,
                    CreatedAt = idea.CreatedAt,
                    ApprovedAt = idea.ApprovedAt
                };
            return View(ideaDetailsModel);

            }
            catch(Exception ex)
            {
                Log.Error("Error while retriving idea details!", ex);
                return RedirectToAction("Index");
            }   

        }

        // GET: Ideas/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ideas/Create
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(IdeaViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

                    if (user == null) return RedirectToAction("Index");

                    Random random = new Random();
                    int imageIndex = random.Next(1, 5);

                    context.Ideas.Add(new Idea { UserID = user.Id, Name = model.Name, Supplies = model.Supplies, Description = model.Description, ImageIndex = imageIndex, CreatedAt = DateTime.Now });

                    await context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    Log.Error("Error while creating idea!", ex);
                    return RedirectToAction("Index");
                }
               
            }
           
             return View(model);
            
        }

        // GET: Ideas/Edit/5
        [Authorize(Roles = "Owner")]
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Ideas/Edit/5
        [HttpPost]
        [Authorize(Roles = "Owner")]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Ideas/Approve/5
        [Authorize(Roles = "Owner, Admin")]
        public ActionResult Approve(int id)
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Owner, Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Approve(int id, FormCollection collection)
        {
            return View();
        }

        // GET: Ideas/Delete/5
        [Authorize(Roles = "Owner, Admin")]
        public async Task<ActionResult> Delete(int id=0)
        {
            try
            {
                Idea idea = await context.Ideas.FindAsync(id);

                if (idea == null) return RedirectToAction("Index");

                ICollection<ApplicationUser> users = UserManager.Users.ToList();
                ApplicationUser owner = (from user
                                         in users
                                         where user.Id == idea.UserID
                                         select user)
                                         .FirstOrDefault();
                

                IdeaDetailsViewModel ideaDetailsModel = new IdeaDetailsViewModel
                {
                    IdeaID = idea.IdeaID,
                    Name = idea.Name,
                    OwnerName = owner.Name,
                    CreatedAt = idea.CreatedAt,
                    ApprovedAt = idea.ApprovedAt
                };
                return View(ideaDetailsModel);

            }
            catch (Exception ex)
            {
                Log.Error("Error while retriving idea details!", ex);
                return RedirectToAction("Index");
            }
        }

        // POST: Ideas/Delete/5
        [HttpPost]
        [Authorize(Roles = "Owner, Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            try
            {
                Idea idea = await context.Ideas.Where(_idea => _idea.IdeaID == id).FirstOrDefaultAsync();
                if (idea != null)
                {
                    context.Ideas.Remove(idea);
                    await context.SaveChangesAsync();
                }
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                Log.Error("Could not delete the selected idea!", ex);
                return RedirectToAction("Index");
            }
        }


        // GET: Ideas/Vote/5
        [Authorize]
        public async Task<ActionResult> Vote(int id = 0)
        {
            try
            {
                Idea idea = await context.Ideas.FindAsync(id);

                if (idea == null) return RedirectToAction("Index");

                ApplicationUser owner = await UserManager.Users.Where(_user => _user.Id == idea.UserID).FirstOrDefaultAsync();

                ApplicationUser userWhoVoated = await UserManager.FindByIdAsync(User.Identity.GetUserId());

                Vote vote = await context.Votes.Where(_vote => _vote.UserID == userWhoVoated.Id && _vote.IdeaID == idea.IdeaID).FirstOrDefaultAsync();

                VoteViewModel voteViewModel = new VoteViewModel
                {
                    IdeaID = idea.IdeaID,
                    Name = idea.Name,
                    OwnerName = owner.Name,
                    CreatedAt = idea.CreatedAt,
                    ApprovedAt = idea.ApprovedAt,
                    Voted = (vote != null) ? true : false                    
                };
                return View(voteViewModel);

            }
            catch (Exception ex)
            {
                Log.Error("Error while retriving vote details!", ex);
                return RedirectToAction("Index");
            }
        }

        // POST: Ideas/Vote/5
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Vote(int id, FormCollection collection)
        {
            try
            {
                Idea idea = await context.Ideas.FindAsync(id);

                if (idea == null) return RedirectToAction("Index");

                ApplicationUser userWhoVoated = await UserManager.FindByIdAsync(User.Identity.GetUserId());

                Vote vote = await context.Votes.Where(_vote => _vote.UserID == userWhoVoated.Id && _vote.IdeaID == idea.IdeaID).FirstOrDefaultAsync();

                if (vote == null)
                {
                    context.Votes.Add(new Vote { UserID = userWhoVoated.Id, IdeaID = idea.IdeaID });
                    await context.SaveChangesAsync();
                }
                else
                {
                    Log.Warning("You alredy voated!");
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Log.Error("Problem while voating!", ex);
                return RedirectToAction("Index");
            }
        }
    }
}
