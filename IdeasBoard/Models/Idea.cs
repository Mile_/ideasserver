﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IdeasBoard.Models
{
    public class Idea
    {
        public int IdeaID { get; set; }
        public string UserID { get; set; }
        public string Name { get; set; }
        public int ImageIndex { get; set; }
        public string Description { get; set; }
        public string Supplies { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ApprovedAt { get; set; }
        public string ApproveMessage { get; set; }

        public virtual ApplicationUser User{get; set;}
        public virtual ICollection<Vote> Votes { get; set; }
    }
}