﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdeasBoard.Models
{
    public class Vote
    {
        public int VoteID { get; set; }
        [Index("IX_UserAndIdea", 1, IsUnique = true)]
        public string UserID { get; set; }
        [Index("IX_UserAndIdea", 2, IsUnique = true)]
        public int IdeaID { get; set; }
        public DateTime VotedOn { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual Idea Idea { get; set; }
    }
}