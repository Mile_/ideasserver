﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IdeasBoard.ViewModels
{
    public class VoteViewModel
    {
        public int IdeaID { get; set; }
        public string OwnerName { get; set; }
        public string Name { get; set; }
        public int VoteCount { get; set; }
        public DateTime CreatedAt { get; set; }          
        public DateTime? ApprovedAt { get; set; }          
        public bool Voted { get; set; }

    }
}