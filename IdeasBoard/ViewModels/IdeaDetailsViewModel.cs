﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;



namespace IdeasBoard.ViewModels
{
    public class IdeaUserDetails
    {
        public string UserName { get; set; }
        public DateTime VotedOn { get; set; }
    }

    public class IdeaDetailsViewModel
    {
        public int IdeaID { get; set; }
        [Display(Name = " Owner")]
        public string OwnerName { get; set; }
        public string OwnerEmail { get; set; }
        public string Name { get; set; }
        public string Supplies { get; set; }
        public string Description { get; set; }
        [Display(Name = " Number of votes")]
        public int VoteCount { get; set; }
        [Display(Name = " Created at")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = " Approved at")]
        public DateTime? ApprovedAt { get; set; }
        [Display(Name = " Votes")]
        public ICollection<IdeaUserDetails> UsersWhoVoted { get; set; }
        public string ApproveMessage { get; set; }

    }
}