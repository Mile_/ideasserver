﻿using System.ComponentModel.DataAnnotations;


namespace IdeasBoard.ViewModels
{
    public class IdeaViewModel
    {
        [Required]
        [Display(Name = "Idea name")]
        [StringLength(30, ErrorMessage = "The {0} must be at minimum {2} and maximum 30 characters long.", MinimumLength = 3)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Supplies")]
        public string Supplies { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
    }
}