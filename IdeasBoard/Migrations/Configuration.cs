﻿namespace IdeasBoard.Migrations
{
    using IdeasBoard.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<IdeasBoard.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(IdeasBoard.Models.ApplicationDbContext context)
        {
            string[] roles = new string[] { "Owner", "Admin", "User"};
           
            foreach (string roleName in roles)
            {
                //RoleStore<IdentityRole> roleStore = new RoleStore<IdentityRole>(context);

                if (!context.Roles.Any(r => r.Name == roleName))
                {
                    RoleStore<ApplicationRole> store = new RoleStore<ApplicationRole>(context);
                    RoleManager<ApplicationRole> manager = new RoleManager<ApplicationRole>(store);
                    ApplicationRole newRole = new ApplicationRole { Name = roleName };

                    manager.Create(newRole);
                }
            }

            if (!context.Users.Any(u => u.UserName == "ideasmainserver@gmail.com"))
            {
                UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
                UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>(store);
                ApplicationUser user = new ApplicationUser { UserName = "ideasmainserver@gmail.com", Email = "ideasmainserver@gmail.com", Name="Admin", RegistredOn = DateTime.Now };

                manager.Create(user, "Admin@15");
                manager.AddToRole(user.Id, "Owner");
            }

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}
