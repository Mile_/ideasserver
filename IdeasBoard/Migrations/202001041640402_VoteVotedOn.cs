﻿namespace IdeasBoard.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VoteVotedOn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Votes", "VotedOn", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Votes", "VotedOn");
        }
    }
}
