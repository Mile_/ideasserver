﻿namespace IdeasBoard.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdeaSuppliesAndApproveMsg : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ideas", "Supplies", c => c.String());
            AddColumn("dbo.Ideas", "ApproveMessage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ideas", "ApproveMessage");
            DropColumn("dbo.Ideas", "Supplies");
        }
    }
}
