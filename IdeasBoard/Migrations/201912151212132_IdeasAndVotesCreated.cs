﻿namespace IdeasBoard.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdeasAndVotesCreated : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ideas",
                c => new
                    {
                        IdeaID = c.Int(nullable: false, identity: true),
                        UserID = c.String(maxLength: 128),
                        Name = c.String(),
                        Description = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        ApprovedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.IdeaID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Votes",
                c => new
                    {
                        VoteID = c.Int(nullable: false, identity: true),
                        UserID = c.String(maxLength: 128),
                        IdeaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.VoteID)
                .ForeignKey("dbo.Ideas", t => t.IdeaID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => new { t.UserID, t.IdeaID }, unique: true, name: "IX_UserAndIdea");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Votes", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Votes", "IdeaID", "dbo.Ideas");
            DropForeignKey("dbo.Ideas", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.Votes", "IX_UserAndIdea");
            DropIndex("dbo.Ideas", new[] { "UserID" });
            DropTable("dbo.Votes");
            DropTable("dbo.Ideas");
        }
    }
}
