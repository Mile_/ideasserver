﻿namespace IdeasBoard.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdeaImageIndexColumnAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ideas", "ImageIndex", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ideas", "ImageIndex");
        }
    }
}
